import { Component, OnInit, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';//Se deben importar para la construcción del formulario y las validaciones.

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy, OnChanges {//Se hace la implementación de las librerías.
  public cambios: Boolean = false;
  public formBuilder = new FormBuilder();
  public message: String = "";
  public errors = [];
  public test: string = "";
  //Se establece que los campos del formulario son obligatorios.
  public loginForm: FormGroup = this.formBuilder.group({
    username: ['', [Validators.required, Validators.minLength(6), (control: FormControl) => {
      if (control.value.length > 12) {
        return {
          maxLength: {
            value: control.value,
            message: 'Error máximo de longitud.'
          }
        };
      }
      else {
        return null;
      }
    }]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  });
  constructor() {
    console.log('Constructor');
  }

  ngOnInit() {
    setTimeout(() => {
      console.log('Entró');
      this.cambios = true;
    }, 1000);
    console.log('onInit');
  }

  ngOnChanges(change: SimpleChanges) {
    console.log(change);
  }

  ngOnDestroy() {
    console.log('onDestroy');
  }

  public submitLogin(form) {
    this.message = '';
    this.errors = [];
    if (this.loginForm.valid) {
      this.message = null;
    }
    else {
      const errors = [];
      Object.keys(this.loginForm.controls).forEach((field) => {
        const control = this.loginForm.get(field);

        control.markAsTouched({ onlySelf: true });
        control.errors ? errors.push(control.errors) : null;
      });

      this.loginForm.updateValueAndValidity({ onlySelf: true, emitEvent: true });
      console.log(errors);
      errors.forEach((_error: any) => {
        if ('required' in _error) {
          if (!this.message) {
            this.message = 'Complete los campos requeridos';
            this.errors.push('Complete los campos requeridos');
          }
          else{
            if(!this.message.includes('Complete los campos requeridos'))
            {
              this.message = this.message.concat('<br>', 'Complete los campos requeridos');
              this.errors.push('Complete los campos requeridos');
            }
          }
        }
        else if('maxLength' in _error)
        {
          if (this.message) {
            this.message = this.message.concat('<br>', _error.maxLength.message);
          }
          else{
            this.message = _error.maxLength.message;
          }
          this.errors.push(_error.maxLength.message);
        }
      });
    }
  }

}
